package ru.webant.baseapplication

import android.app.Application
import android.content.Intent
import io.realm.Realm
import io.realm.RealmConfiguration
import ru.webant.baseapplication.dagger.AppComponent
import ru.webant.baseapplication.dagger.DaggerAppComponent
import ru.webant.baseapplication.dagger.modules.ApplicationModule
import ru.webant.baseapplication.ui.activity.LoginActivity
import ru.webant.baseapplication.util.SharedPreference

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        SharedPreference.init(this)
        Realm.init(this)

        val config = RealmConfiguration.Builder().name("database").build()
        Realm.setDefaultConfiguration(config)

        appComponent = DaggerAppComponent.builder().applicationModule(ApplicationModule(this)).build()
    }

    fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
    }
}