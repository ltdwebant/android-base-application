package ru.webant.baseapplication.network

import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import ru.webant.baseapplication.App
import ru.webant.baseapplication.util.TokenUtil
import java.io.IOException
import javax.inject.Inject

class TokenAuthenticator : Authenticator {

    @Inject lateinit var authApi: AuthApi
    @Inject lateinit var app: App

    var hasInit = false

    override fun authenticate(route: Route?, response: Response?): Request? {
        var token: String? = null
        val responseBody = String(response?.body()?.bytes() ?: throw IOException("Empty network response."))

        if (!hasInit) {
            App.appComponent.inject(this)
            hasInit = true
        }

        val needRefreshToken = responseBody.contains("token provided has expired")
        val refreshToken = TokenUtil.getRefreshToken()

        if (refreshToken == null && needRefreshToken) {
            TokenUtil.clearAccessToken()
            app.startLoginActivity()
            return null
        } else if (refreshToken != null && needRefreshToken) {
            val refreshTokenResponse = authApi.getRefreshToken(MY_CLIENT_ID, "refresh_token", "$refreshToken", MY_CLIENT_SECRET).execute()
            if (refreshTokenResponse.isSuccessful)
                token = TokenUtil.saveAccessToken(refreshTokenResponse.body())
        }

        if (token == null)
            throw IOException(responseBody)
        else
            return response.request()?.newBuilder()
                    ?.header("Authorization", "Bearer $token")
                    ?.build()
    }
}