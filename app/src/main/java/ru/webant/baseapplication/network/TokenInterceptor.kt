package ru.webant.baseapplication.network

import okhttp3.Interceptor
import okhttp3.Response
import ru.webant.baseapplication.util.TokenUtil

class TokenInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response? {
        val token = TokenUtil.getAccessToken()
        return if (token != null)
            chain?.proceed(chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer $token")
                    .build())
        else
            chain?.proceed(chain.request())
    }
}