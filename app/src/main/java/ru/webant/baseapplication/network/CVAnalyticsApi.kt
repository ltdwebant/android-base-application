package ru.webant.baseapplication.network

import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import ru.webant.baseapplication.model.ListResponse
import ru.webant.baseapplication.model.PlaceModel
import rx.Observable

interface CVAnalyticsApi {


    @POST("/api/v1/places")
    fun createPlace(@Body place: PlaceModel): Observable<PlaceModel>

    @GET("/api/v1/places")
    fun getPlaces(@Query("start") lastItemId: Int, @Query("limit") limit: Int): Observable<ListResponse<PlaceModel>>
}