package ru.webant.baseapplication.network

import retrofit2.Call
import retrofit2.http.*
import ru.webant.baseapplication.model.Token
import ru.webant.baseapplication.model.User
import rx.Observable


val MY_CLIENT_ID = "10_2lu7gnw4fxc0sk8w4scwsggwwkok0ggkkcc8c08g8g8k00ksww"
val MY_CLIENT_SECRET = "6cqt45v80oowk80sokwoo4g8w8sgo8k8sgg8k4c0sw0oksso04"

interface AuthApi {

    @FormUrlEncoded
    @Headers(
            "Content-Type:application/x-www-form-urlencoded",
            "Accept:application/json, text/plain, */*"
    )
    @POST("/oauth/v2/token")
    fun getToken(@Field("client_id") client_id: String,
                 @Field("grant_type") grant_type: String,
                 @Field("username") username: String,
                 @Field("password") password: String,
                 @Field("client_secret") client_secret: String): Observable<Token>

    @Headers("Accept: application/json, text/plain, */*")
    @FormUrlEncoded
    @POST("/oauth/v2/token")
    fun getRefreshToken(
            @Field("client_id") client_id: String?,
            @Field("grant_type") grant_type: String?,
            @Field("refresh_token") refresh_token: String?,
            @Field("client_secret") client_secret: String?): Call<Token>

    @POST("/api/v1/users")
    fun createUser(@Body body: User): Observable<User>

}