package ru.webant.baseapplication.ui.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import ru.webant.baseapplication.R
import ru.webant.baseapplication.presentation.presenter.MainActivityPresenter
import ru.webant.baseapplication.presentation.view.MainActivityView

class MainActivity : MvpAppCompatActivity(), MainActivityView {

    @InjectPresenter(type = PresenterType.GLOBAL)
    lateinit var presenter: MainActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())

//        startActivity(Intent(this, PlaceListActivity::class.java))

    }

    override fun setActiveFragment(fragment: Fragment) {
        Log.d(this::class.java.simpleName, "setActiveFragment: fragment = $fragment")
        supportFragmentManager.beginTransaction().replace(R.id.rootView, fragment).commit()
    }
}
