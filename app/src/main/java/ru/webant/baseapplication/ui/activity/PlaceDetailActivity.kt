package ru.webant.baseapplication.ui.activity

import android.os.Bundle

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_detail.*

import ru.webant.baseapplication.R
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.presentation.presenter.PlaceDetailPresenter
import ru.webant.baseapplication.presentation.view.PlaceDetailView
import ru.webant.baseapplication.util.Const.ITEM_MODEL_EXTRA

class PlaceDetailActivity : MvpAppCompatActivity(), PlaceDetailView {
    override fun showLoadItemError() {
        detailTextView.setText(R.string.loadItemError)
    }

    override fun showItem(place: PlaceModel) {
        detailTextView.text = place.toString()
    }

    @InjectPresenter
    lateinit var mPlaceDetailPresenter: PlaceDetailPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        if (mPlaceDetailPresenter.itemId == null)
            mPlaceDetailPresenter.itemId = intent.getIntExtra(ITEM_MODEL_EXTRA, 0)
    }

    companion object {
        val TAG = "PlaceDetailActivity"
    }
}
