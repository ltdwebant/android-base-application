package ru.webant.baseapplication.ui.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import kotlinx.android.synthetic.main.fragment_place_list.*
import ru.webant.baseapplication.R
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.presentation.presenter.PlaceListPresenter
import ru.webant.baseapplication.presentation.view.PlaceListView
import ru.webant.baseapplication.ui.adapter.PlacesAdapter

class PlaceListFragment : MvpAppCompatFragment(), PlaceListView {
    override fun enableSwipeDownRefreshAction(enable: Boolean) {
        swipeRefreshLayout.direction = if (enable) SwipyRefreshLayoutDirection.BOTH else SwipyRefreshLayoutDirection.TOP
    }

    @InjectPresenter(type = PresenterType.GLOBAL)
    lateinit var placeListPresenter: PlaceListPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater?.inflate(R.layout.fragment_place_list, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        swipeRefreshLayout.setOnRefreshListener { direction ->
            val items = (placesRecycleView.adapter as PlacesAdapter).items
            if (direction == SwipyRefreshLayoutDirection.TOP) {
                /* тянем вверх и обновляем текущие item'ы */
                placeListPresenter.onRefresh(items.size)
            } else {
                /* тянем вниз и подгружаем новые item'ы*/
                if (items.isNotEmpty())
                    placeListPresenter.loadMoreItems(items.size)
            }
        }
    }

    override fun updateRefreshStatus(refreshInProgress: Boolean) {
        swipeRefreshLayout.isRefreshing = refreshInProgress
    }

    override fun updateLoadingStatus(loadingInProgress: Boolean) {
        swipeRefreshLayout.isRefreshing = loadingInProgress
    }

    override fun addItems(items: ArrayList<PlaceModel>?) {
        if (items != null)
            (placesRecycleView.adapter as? PlacesAdapter)?.addItems(items)
    }

    override fun setItems(items: ArrayList<PlaceModel>?) {
        Log.d(this::class.java.simpleName, "setItems: count = ${items?.size}")
        if (items != null) {
            if (placesRecycleView.adapter == null) {
                val adapter = PlacesAdapter(mvpDelegate, { id ->
                    Log.i(this::class.java.simpleName, "setItems: on item click: $id")
                }, {
                    placeListPresenter.loadMoreItems(it)
                })

                adapter.items = items
                placesRecycleView.adapter = adapter
            } else {
                (placesRecycleView.adapter as PlacesAdapter).items = items
            }
        }
    }

    override fun showError(messageId: Int) {
        Snackbar.make(swipeRefreshLayout, messageId, Snackbar.LENGTH_LONG)
    }

}