package ru.webant.baseapplication.ui.adapter

//abstract class MvpBaseViewHolder<T : MvpPresenter<out MvpView>, M : IModelId>(val view: View) : RecyclerView.ViewHolder(view) {
//
//    @InjectPresenter
//    lateinit var presenter: T
//    lateinit var item: M
//
//    @ProvidePresenter
//    abstract fun providePresenter(): T
//
//    var mMvpDelegate: MvpDelegate<out Any>? = null
//
//    fun bind(parentDelegate: MvpDelegate<out Any>, item: M) {
//        mMvpDelegate?.deattachHolder()
//        this.item = item
//        mMvpDelegate = MvpDelegate(this)
//        mMvpDelegate?.attachHolder(parentDelegate, getHolderTag(item))
//        onHolderBind(item)
//    }
//
//    abstract fun onHolderBind(item: M)
//
//    abstract fun getHolderTag(item: M): String?
//
//}
//
//interface IModelId {
//    var id: Int
//}