package ru.webant.baseapplication.ui.adapter

import android.support.v7.widget.RecyclerView
import com.arellomobile.mvp.MvpDelegate

abstract class MvpBaseAdapter<T : RecyclerView.ViewHolder>(private val mParentDelegate: MvpDelegate<*>, private val mChildId: String) : RecyclerView.Adapter<T>() {
    val mMvpDelegate: MvpDelegate<out Any> by lazy {
        MvpDelegate(this).apply {
            setParentDelegate(mParentDelegate, mChildId)
        }
    }


    init {
        mMvpDelegate.onCreate()
    }

}