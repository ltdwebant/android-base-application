package ru.webant.baseapplication.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import kotlinx.android.synthetic.main.activity_place_list.*
import ru.webant.baseapplication.R
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.presentation.presenter.PlaceListPresenter
import ru.webant.baseapplication.presentation.view.PlaceListView
import ru.webant.baseapplication.ui.adapter.PlacesAdapter
import ru.webant.baseapplication.util.Const

class PlaceListActivity : MvpAppCompatActivity(), PlaceListView {

    override fun enableSwipeDownRefreshAction(enable: Boolean) {
        swipeRefreshLayout.direction = if (enable) SwipyRefreshLayoutDirection.BOTH else SwipyRefreshLayoutDirection.TOP
    }

    @InjectPresenter(type = PresenterType.GLOBAL)
    lateinit var placeListPresenter: PlaceListPresenter
    private var placesAdapter: PlacesAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_list)
        swipeRefreshLayout.setOnRefreshListener { direction ->
            val items = placesAdapter?.items
            if (direction == SwipyRefreshLayoutDirection.TOP) {
                /* тянем вверх и обновляем текущие item'ы */
                placeListPresenter.onRefresh(items?.size)
            } else {
                /* тянем вниз и подгружаем новые item'ы*/
                if (items?.isNotEmpty() == true)
                    placeListPresenter.loadMoreItems(items.size)
            }
        }

    }

    override fun updateRefreshStatus(refreshInProgress: Boolean) {
        swipeRefreshLayout.isRefreshing = refreshInProgress
    }

    override fun updateLoadingStatus(loadingInProgress: Boolean) {
        swipeRefreshLayout.isRefreshing = loadingInProgress
    }

    override fun addItems(items: ArrayList<PlaceModel>?) {
        if (items != null)
            placesAdapter?.addItems(items)
    }

    override fun setItems(items: ArrayList<PlaceModel>?) {
        if (items != null) {
            if (placesAdapter == null) {
                placesAdapter = PlacesAdapter(mvpDelegate, { id ->
                    Log.i(this::class.java.simpleName, "setItems: on item click: $id")
                    startActivity(Intent(this, PlaceDetailActivity::class.java).putExtra(Const.ITEM_MODEL_EXTRA, id))
                }, {
                    placeListPresenter.loadMoreItems(it)
                })

                placesAdapter?.items = items
                placesRecycleView.adapter = placesAdapter
            } else {
                placesAdapter?.items = items
            }
        }
    }

    override fun showError(messageId: Int) {
        Snackbar.make(swipeRefreshLayout, messageId, Snackbar.LENGTH_LONG).show()
    }
}
