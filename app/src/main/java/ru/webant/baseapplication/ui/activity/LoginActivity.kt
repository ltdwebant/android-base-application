package ru.webant.baseapplication.ui.activity

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import kotlinx.android.synthetic.main.activity_login.*
import ru.webant.baseapplication.R
import ru.webant.baseapplication.presentation.presenter.LoginPresenter
import ru.webant.baseapplication.presentation.view.LoginView
import ru.webant.baseapplication.util.visible

class LoginActivity : MvpAppCompatActivity(), LoginView {

    @InjectPresenter(type = PresenterType.GLOBAL)
    lateinit var mLoginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        applyButton.setOnClickListener { mLoginPresenter.onApplyButtonClick(emailEditText.text.toString(), passwordEditText.text.toString()) }
        changeActivityMode.setOnClickListener { mLoginPresenter.onChangeModeStateClick() }
        restorePassword.setOnClickListener { mLoginPresenter.onRestorePasswordClick() }
    }

    override fun updateActivityMode(isRegistrationMode: Boolean) {
        if (isRegistrationMode) {
            changeActivityMode.text = getString(R.string.log_in)
            titleTextView.text = getString(R.string.registration)
            applyButton.text = getString(R.string.create_account)
        } else {
            changeActivityMode.text = getString(R.string.registration)
            titleTextView.text = getString(R.string.log_in)
            applyButton.text = getString(R.string.do_login)
        }
        restorePassword.visible(!isRegistrationMode)
    }

    override fun setErrorMessage(message: Int) {
        loginErrorMessage.text = getString(message)
    }

    override fun updateInputErrorsState(loginError: Int, passwordError: Int) {
        if (loginError != 0) {
            emailTextInput.error = getString(loginError)
        } else {
            emailTextInput.isErrorEnabled = false
        }

        if (passwordError != 0) {
            passwordTextInput.error = getString(passwordError)
        } else {
            passwordTextInput.isErrorEnabled = false
        }
    }

    override fun updateProgressState(loading: Boolean) {
        applyButton.visible(!loading)
        progressBar.visible(loading)
    }

    override fun onEnterComplete() {
        startActivity(Intent(this, MainActivity::class.java))
    }

}
