package ru.webant.baseapplication.ui.adapter.holder

import android.support.v7.widget.RecyclerView
import android.view.View

import com.arellomobile.mvp.MvpDelegate

abstract class MvpViewHolder(private val mParentDelegate: MvpDelegate<*>, itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected var mvpDelegate: MvpDelegate<*>? = null

    protected fun destroyMvpDelegate() {
        mvpDelegate?.onSaveInstanceState()
        mvpDelegate?.onDetach()
        mvpDelegate = null
    }

    protected fun createMvpDelegate(mvpChildId: String) {
        mvpDelegate = MvpDelegate(this)
        mvpDelegate?.setParentDelegate(mParentDelegate, mvpChildId)
        mvpDelegate?.onCreate()
        mvpDelegate?.onAttach()
    }

}