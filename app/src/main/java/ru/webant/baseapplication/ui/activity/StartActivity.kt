package ru.webant.baseapplication.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.webant.baseapplication.R
import ru.webant.baseapplication.util.TokenUtil.getAccessToken

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        val flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP

        if (getAccessToken() == null)
            startActivity(Intent(this, LoginActivity::class.java).setFlags(flags))
        else
            startActivity(Intent(this, MainActivity::class.java).setFlags(flags))
    }
}
