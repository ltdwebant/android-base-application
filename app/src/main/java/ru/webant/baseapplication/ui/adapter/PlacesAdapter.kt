package ru.webant.baseapplication.ui.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpDelegate
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.item_place.view.*
import ru.webant.baseapplication.R
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.presentation.presenter.ItemPlacePresenter
import ru.webant.baseapplication.presentation.view.ItemPlaceView
import ru.webant.baseapplication.ui.adapter.holder.MvpViewHolder

class PlacesAdapter(parentDelegate: MvpDelegate<Any>, val onItemSelect: (Int) -> Unit, val onBottomScrolled: (Int) -> Unit) : MvpBaseAdapter<PlacesAdapter.ViewHolder>(parentDelegate, 0.toString()) {

    private var onBottomScrolledCalled = false
    var items = ArrayList<PlaceModel>()
        set(value) {
            field = value
            notifyDataSetChanged()
            onBottomScrolledCalled = false
        }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])

        if (position > items.size - 4 && !onBottomScrolledCalled) {
            onBottomScrolled.invoke(items.size)
            onBottomScrolledCalled = true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int)
            = ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_place, parent, false))

    fun addItems(items: List<PlaceModel>) {
        if (items.isNotEmpty()) {
            this.items.addAll(items)
            notifyDataSetChanged()
            onBottomScrolledCalled = false
        }
    }

    inner class ViewHolder(val view: View) : MvpViewHolder(mMvpDelegate, view), ItemPlaceView {

        override fun setBackgroundColor(color: Int) {
            if (color == Color.TRANSPARENT)
                view.background.clearColorFilter()
            else {
                view.background.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
            }
            view.invalidate()
        }

        @InjectPresenter(type = PresenterType.LOCAL)
        lateinit var presenter: ItemPlacePresenter
        lateinit var item: PlaceModel

        @ProvidePresenter
        fun providePresenter() = ItemPlacePresenter(item)

        fun bind(item: PlaceModel) {
            destroyMvpDelegate()
            this.item = item
            createMvpDelegate(item.id?.toString()!!)

            with(view) {
                titleTextView.text = item.name
                descriptionTextView.text = item.description
                setOnClickListener {
                    onItemSelect.invoke(item.id!!)
//                    presenter.onViewClicked()
                }
            }
        }
    }
}