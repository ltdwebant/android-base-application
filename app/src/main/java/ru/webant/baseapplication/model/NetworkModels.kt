package ru.webant.baseapplication.model

data class Token(var access_token: String, var refresh_token: String)

data class User(val id: Int, val username: String, val email: String, val password: String)

data class ListResponse<T>(var count: Int, var items: ArrayList<T>)