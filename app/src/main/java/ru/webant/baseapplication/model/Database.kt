package ru.webant.baseapplication.model

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.Realm
import io.realm.RealmObject
import ru.webant.baseapplication.App
import ru.webant.baseapplication.util.SharedPreference
import javax.inject.Inject

class Database {

    @Inject lateinit var gson: Gson
    private val realm: Realm by lazy { Realm.getDefaultInstance() }
    private val ITEM_SYNC_CONFIG_PREF = "items"
    private val itemsSyncConfig: HashMap<String, Int>? = null
        get() {
            if (field == null) {
                val type = object : TypeToken<HashMap<String, Int>>() {}.type
                val fromJson = gson.fromJson<HashMap<String, Int>?>(SharedPreference.read(ITEM_SYNC_CONFIG_PREF), type) ?: HashMap()
                field = fromJson
            }
            return field
        }

    init {
        App.appComponent.inject(this)
    }

    fun <T : RealmObject> createOrUpdate(model: T, onComplete: (() -> Unit)? = null, onError: ((Throwable) -> Unit)? = null) {
        realm.executeTransactionAsync({ it.copyToRealmOrUpdate(model) }, onComplete, onError)
    }

    fun <T : RealmObject> createOrUpdate(models: List<T>, onComplete: (() -> Unit)? = null, onError: ((Throwable) -> Unit)? = null) {
        realm.executeTransactionAsync({ it.copyToRealmOrUpdate(models) }, onComplete, onError)
    }

    fun <T : RealmObject> createOrUpdate(itemsHolder: ListResponse<T>, onComplete: (() -> Unit)? = null, onError: ((Throwable) -> Unit)? = null) {
        if (itemsHolder.items.isNotEmpty()) {
            setTotalItemsCount(itemsHolder.items.first().javaClass.simpleName, itemsHolder.count)
            realm.executeTransactionAsync({ it.copyToRealmOrUpdate(itemsHolder.items) }, onComplete, onError)
        }
    }

    fun <T : RealmObject> getAll(clazz: Class<T>) = realm.where(clazz).findAll()

    fun <T : RealmObject> findById(clazz: Class<T>, id: Int): T? = realm.where(clazz).equalTo("id", id).findFirst()


    fun setTotalItemsCount(tag: String, count: Int) =
            itemsSyncConfig?.put(tag, count)

    fun getTotalItemsCount(tag: String): Int {
        if (itemsSyncConfig?.contains(tag) == true)
            return itemsSyncConfig?.get(tag) ?: -2
        else
            return -1
    }

    fun close() {
        realm.close()
        SharedPreference.save(ITEM_SYNC_CONFIG_PREF, gson.toJson(itemsSyncConfig))
    }

}