package ru.webant.baseapplication.model

import android.graphics.Color
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class PlaceModel(
        @PrimaryKey var id: Int? = null,
        var name: String? = null,
        var description: String? = null,
        var address: String? = null,
        var lat: Float? = null,
        var lng: Float? = null
) : RealmObject(), Serializable {

    @Ignore
    var color: Int = Color.TRANSPARENT

    override fun toString(): String {
        return "PlaceModel(id=$id, name=$name, description=$description, address=$address, lat=$lat, lng=$lng)"
    }
}

