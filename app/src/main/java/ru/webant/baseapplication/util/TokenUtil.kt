package ru.webant.baseapplication.util

import ru.webant.baseapplication.model.Token

object TokenUtil {
    private val REFRESH_TOKEN_PREF = "refresh_token"
    private val ACCESS_TOKEN_PREF = "access_token"

    fun getAccessToken() = SharedPreference.read(ACCESS_TOKEN_PREF)

    fun getRefreshToken() = SharedPreference.read(REFRESH_TOKEN_PREF)

    fun saveAccessToken(tokenResponse: Token): String {
        SharedPreference.save(ACCESS_TOKEN_PREF, tokenResponse.access_token)
        SharedPreference.save(REFRESH_TOKEN_PREF, tokenResponse.refresh_token)
        return tokenResponse.access_token
    }

    fun clearAccessToken() {
        SharedPreference.clear(ACCESS_TOKEN_PREF)
        SharedPreference.clear(REFRESH_TOKEN_PREF)
    }
}