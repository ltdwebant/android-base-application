package ru.webant.baseapplication.util

import android.view.View

fun View?.hide(isGone: Boolean? = true) {
    this?.visibility = if (isGone ?: true) View.GONE else View.INVISIBLE
}

fun View?.show() {
    this?.visibility = View.VISIBLE
}

fun View?.visible(isVisible: Boolean) {
    this?.visibility = if (isVisible) View.VISIBLE else View.GONE
}