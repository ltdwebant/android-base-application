package ru.webant.baseapplication.util

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v4.content.ContextCompat

fun Activity.pxToDp(px: Float): Float = px / resources.displayMetrics.density

fun Activity.dpToPx(dp: Float): Float = dp * resources.displayMetrics.density

fun Context.vectorDrawable(resId: Int): Drawable = VectorDrawableCompat.create(this.resources, resId, null) as Drawable

fun Context.drawable(resId: Int): Drawable = ContextCompat.getDrawable(this, resId)

fun Context.isGpsEnable(): Boolean {
    val mgr = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return mgr.isProviderEnabled(LocationManager.GPS_PROVIDER)
}

fun Context.createColoredVector(resId: Int, color: Int): Drawable = createColoredVector(resources, resId, color)