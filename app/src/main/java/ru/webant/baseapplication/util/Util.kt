package ru.webant.baseapplication.util

import android.os.Handler
import kotlin.concurrent.thread


fun <T> makeAsync(background: () -> T, ui: (T) -> Unit) {
    val handler = Handler()
    thread {
        val result = background()
        handler.post { ui(result) }
    }.start()
}