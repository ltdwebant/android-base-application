package ru.webant.baseapplication.util

import com.arellomobile.mvp.MvpDelegate

fun MvpDelegate<out Any>.attachHolder(parentDelegate: MvpDelegate<out Any>, tag: String?) {
    setParentDelegate(parentDelegate, tag)
    onCreate()
    onAttach()
}

fun MvpDelegate<out Any>.deattachHolder() {
    onSaveInstanceState()
    onDetach()
    onDestroyView()

}