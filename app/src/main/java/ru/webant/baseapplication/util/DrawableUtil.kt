package ru.webant.baseapplication.util

import android.content.res.Resources
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.support.graphics.drawable.VectorDrawableCompat


fun createColoredVector(resources: Resources, resId: Int, color: Int): Drawable {
    val drawableCompat = VectorDrawableCompat.create(resources, resId, null)
    drawableCompat?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    return drawableCompat as Drawable
}