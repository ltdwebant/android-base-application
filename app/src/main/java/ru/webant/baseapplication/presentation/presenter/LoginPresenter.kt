package ru.webant.baseapplication.presentation.presenter


import android.util.Log
import com.arellomobile.mvp.InjectViewState
import ru.webant.baseapplication.App
import ru.webant.baseapplication.R
import ru.webant.baseapplication.model.User
import ru.webant.baseapplication.network.AuthApi
import ru.webant.baseapplication.network.MY_CLIENT_ID
import ru.webant.baseapplication.network.MY_CLIENT_SECRET
import ru.webant.baseapplication.presentation.view.LoginView
import ru.webant.baseapplication.util.TokenUtil.saveAccessToken
import javax.inject.Inject

@InjectViewState
class LoginPresenter : BasePresenter<LoginView>() {

    @Inject
    lateinit var authApi: AuthApi

    var isRegistrationMode = true

    var USE_EMAIL_LOGIN = false // username || email login

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Log.d(this::class.java.simpleName, "onFirstViewAttach: ${System.currentTimeMillis()}")
        App.appComponent.inject(this)
    }

    fun register(login: String, password: String) {
        viewState.updateProgressState(loading = true)
        val user = User(0, login, "$login${System.currentTimeMillis()}@gmail.com", password)
        apiRequest(authApi.createUser(user), {
            login(login, password)
        }, {
            checkErrors(it)
        })
    }

    fun login(login: String, password: String) {
        viewState.updateProgressState(loading = true)
        apiRequest(authApi.getToken(MY_CLIENT_ID, "password", login, password, MY_CLIENT_SECRET), {
            saveAccessToken(it)
            viewState.onEnterComplete()
        }, {
            checkErrors(it)
        })
    }

    private fun checkErrors(it: Throwable) {
        viewState.updateProgressState(loading = false)
        val message = it.message
        Log.e(this::class.java.simpleName, "checkErrors: message = $message")
        viewState.setErrorMessage(
                if (message?.contains("Unable to resolve host") == true)
                    R.string.serverConnectionError
                else if (message?.contains("timeout") == true)
                    R.string.connectionTimeout
                else if (message?.contains("Invalid username") == true)
                    R.string.invalidUserData
                else if (message?.contains("409") == true)
                    R.string.usernameExist
                else
                    R.string.unknownError)
    }

    fun onApplyButtonClick(login: String, password: String) {
        if (!validateParams(login, password))
            return

        if (isRegistrationMode)
            register(login, password)
        else
            login(login, password)
    }

    private fun validateParams(login: String, password: String): Boolean {
        var loginError = 0
        var passwordError = 0

        if (USE_EMAIL_LOGIN && !login.contains(Regex("@\\w+?\\.")))
            loginError = R.string.checkEmailValid

        if (login.isEmpty())
            loginError = R.string.enterLogin

        if (password.isEmpty())
            passwordError = R.string.enterPassword

        viewState.updateInputErrorsState(loginError, passwordError)
        return (loginError == 0 && passwordError == 0)
    }

    fun onRestorePasswordClick() {
    }

    fun onChangeModeStateClick() {
        isRegistrationMode = !isRegistrationMode
        viewState.updateActivityMode(isRegistrationMode)
    }
}