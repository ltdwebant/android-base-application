package ru.webant.baseapplication.presentation.view

import com.arellomobile.mvp.MvpView


interface ItemPlaceView : MvpView {
    fun setBackgroundColor(color: Int)
}