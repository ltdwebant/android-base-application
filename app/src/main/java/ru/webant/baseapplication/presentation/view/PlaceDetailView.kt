package ru.webant.baseapplication.presentation.view

import com.arellomobile.mvp.MvpView
import ru.webant.baseapplication.model.PlaceModel

interface PlaceDetailView : MvpView {

    fun showItem(place: PlaceModel)

    fun showLoadItemError()

}