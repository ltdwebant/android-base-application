package ru.webant.baseapplication.presentation.view

import android.support.v4.app.Fragment
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface MainActivityView : MvpView {

    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun setActiveFragment(fragment: Fragment)
}