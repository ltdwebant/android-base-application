package ru.webant.baseapplication.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import ru.webant.baseapplication.App
import ru.webant.baseapplication.R
import ru.webant.baseapplication.model.Database
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.network.CVAnalyticsApi
import ru.webant.baseapplication.presentation.view.PlaceListView
import java.util.*
import javax.inject.Inject

@InjectViewState
class PlaceListPresenter : BasePresenter<PlaceListView>() {

    @Inject lateinit var database: Database
    @Inject lateinit var cvApi: CVAnalyticsApi

    init {
        App.appComponent.inject(this)
    }

    private val onRefreshError by lazy {
        return@lazy { _: Throwable ->
            viewState.updateRefreshStatus(refreshInProgress = false)
            viewState.showError(R.string.fetchDataError)
        }
    }

    private val onFirstLoadError by lazy {
        return@lazy { _: Throwable ->
            viewState.updateLoadingStatus(loadingInProgress = false)
            val itemsList = database.getAll(PlaceModel::class.java)
            viewState.setItems(ArrayList(itemsList))
            viewState.enableSwipeDownRefreshAction(enable = true)
        }
    }

    override fun onFirstViewAttach() {
        viewState.updateLoadingStatus(loadingInProgress = true)
        loadData(onFirstLoadError)
    }

    fun onRefresh(limit: Int? = null) {
        viewState.updateRefreshStatus(refreshInProgress = true)
        loadData(limit = limit ?: 10)
    }

    fun loadMoreItems(start: Int) {
        val total = database.getTotalItemsCount(PlaceModel::class.java.simpleName)
        if (start != total) {
            apiRequest(cvApi.getPlaces(start, 10), {
                database.createOrUpdate(it)
                viewState.updateRefreshStatus(refreshInProgress = false)
                viewState.addItems(it.items)
            }, onRefreshError)
        }
    }

    private fun loadData(onError: (Throwable) -> Unit = onRefreshError, start: Int = 0, limit: Int = 10) {
        apiRequest(cvApi.getPlaces(start, limit), {
            database.createOrUpdate(it)
            viewState.updateRefreshStatus(refreshInProgress = false)
            viewState.enableSwipeDownRefreshAction(enable = false)
            viewState.setItems(it.items)
        }, onError)
    }

    override fun onDestroy() {
        super.onDestroy()
        database.close()
    }

}