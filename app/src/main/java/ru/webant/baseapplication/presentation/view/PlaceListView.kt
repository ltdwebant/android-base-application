package ru.webant.baseapplication.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.webant.baseapplication.model.PlaceModel

interface PlaceListView : MvpView {

    fun updateRefreshStatus(refreshInProgress: Boolean)

    fun updateLoadingStatus(loadingInProgress: Boolean)

    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun setItems(items: ArrayList<PlaceModel>?)

    @StateStrategyType(value = AddToEndSingleStrategy::class)
    fun addItems(items: ArrayList<PlaceModel>?)

    fun showError(messageId: Int)

    fun enableSwipeDownRefreshAction(enable: Boolean)

}