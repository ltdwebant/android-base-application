package ru.webant.baseapplication.presentation.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import ru.webant.baseapplication.presentation.view.MainActivityView
import ru.webant.baseapplication.ui.fragment.PlaceListFragment
import ru.webant.baseapplication.util.makeAsync

@InjectViewState
class MainActivityPresenter : BasePresenter<MainActivityView>() {

    private var placeListFragment: PlaceListFragment? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Log.d(this::class.java.simpleName, "onFirstViewAttach: viewState 1 = $viewState")
        makeAsync({
            placeListFragment = PlaceListFragment()
        }) {
            Log.d(this::class.java.simpleName, "onFirstViewAttach: viewState 2 = $viewState")
            placeListFragment?.let { viewState.setActiveFragment(it) }
        }
    }
}