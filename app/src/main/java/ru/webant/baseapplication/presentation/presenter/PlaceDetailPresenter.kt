package ru.webant.baseapplication.presentation.presenter


import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.webant.baseapplication.App
import ru.webant.baseapplication.model.Database
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.presentation.view.PlaceDetailView
import javax.inject.Inject

@InjectViewState
class PlaceDetailPresenter : MvpPresenter<PlaceDetailView>() {

    init {
        App.appComponent.inject(this)
    }

    var itemId: Int? = null
        set(value) {
            field = value
            if (value != null) {
                Log.d(this::class.java.simpleName, "value = $value")
                val item = database.findById(PlaceModel::class.java, value)
                Log.d(this::class.java.simpleName, "item = $item")
                if (item != null) {
                    viewState.showItem(item)
                } else {
                    viewState.showLoadItemError()
                }
            }
        }
    @Inject lateinit var database: Database

    override fun onFirstViewAttach() {
    }

    override fun onDestroy() {
        super.onDestroy()
        database.close()
    }

}