package ru.webant.baseapplication.presentation.view

import com.arellomobile.mvp.MvpView

interface LoginView : MvpView {

    fun setErrorMessage(message: Int)

    fun updateProgressState(loading: Boolean)

    fun updateActivityMode(isRegistrationMode: Boolean)

    fun updateInputErrorsState(loginError: Int, passwordError: Int)

    fun onEnterComplete()

}