package ru.webant.baseapplication.presentation.presenter

import android.graphics.Color
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.webant.baseapplication.model.PlaceModel
import ru.webant.baseapplication.presentation.view.ItemPlaceView

@InjectViewState
class ItemPlacePresenter(val item: PlaceModel) : MvpPresenter<ItemPlaceView>() {

    override fun toString(): String {
        return "ItemPlacePresenter(item=$item)"
    }

    override fun attachView(view: ItemPlaceView?) {
        super.attachView(view)
        viewState.setBackgroundColor(item.color)
    }

    fun onViewClicked() {
        item.color = Color.BLACK
        viewState.setBackgroundColor(item.color)
    }
}