package ru.webant.baseapplication.dagger.modules

import dagger.Module
import dagger.Provides
import ru.webant.baseapplication.model.Database

@Module
class DatabaseModule {

    @Provides
    fun provideDatabase() = Database()
}