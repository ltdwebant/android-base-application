package ru.webant.baseapplication.dagger

import dagger.Component
import ru.webant.baseapplication.dagger.modules.ApplicationModule
import ru.webant.baseapplication.dagger.modules.AuthApiModule
import ru.webant.baseapplication.dagger.modules.CVAnalyticsApiModule
import ru.webant.baseapplication.dagger.modules.DatabaseModule
import ru.webant.baseapplication.model.Database
import ru.webant.baseapplication.network.TokenAuthenticator
import ru.webant.baseapplication.presentation.presenter.LoginPresenter
import ru.webant.baseapplication.presentation.presenter.PlaceDetailPresenter
import ru.webant.baseapplication.presentation.presenter.PlaceListPresenter
import ru.webant.baseapplication.ui.activity.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AuthApiModule::class, CVAnalyticsApiModule::class, ApplicationModule::class, DatabaseModule::class))
interface AppComponent {
    fun inject(target: LoginPresenter)
    fun inject(target: TokenAuthenticator)
    fun inject(target: MainActivity)
    fun inject(target: PlaceListPresenter)
    fun inject(target: PlaceDetailPresenter)
    fun inject(target: Database)
}