package ru.webant.baseapplication.dagger.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.webant.baseapplication.network.AuthApi
import javax.inject.Singleton

@Module(includes = arrayOf(RetrofitModule::class))
class AuthApiModule {

    @Provides
    @Singleton
    fun provideAuthApi(retrofit: Retrofit): AuthApi = retrofit.create(AuthApi::class.java)
}