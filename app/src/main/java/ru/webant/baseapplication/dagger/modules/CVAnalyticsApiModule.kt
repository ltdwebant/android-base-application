package ru.webant.baseapplication.dagger.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.webant.baseapplication.network.CVAnalyticsApi
import javax.inject.Singleton

@Module(includes = arrayOf(RetrofitModule::class))
class CVAnalyticsApiModule {

    @Provides
    @Singleton
    fun provideCVAnalyticsApi(retrofit: Retrofit): CVAnalyticsApi = retrofit.create(CVAnalyticsApi::class.java)
}