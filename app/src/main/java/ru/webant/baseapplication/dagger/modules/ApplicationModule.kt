package ru.webant.baseapplication.dagger.modules

import dagger.Module
import dagger.Provides
import ru.webant.baseapplication.App
import javax.inject.Singleton

@Module
class ApplicationModule(application: App) {

    var application: App
        @Provides
        @Singleton
        get() = field

    init {
        this.application = application
    }

}